# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.3.7] - 2022-12-13
### Fixed
 - Fix file included in other file name not being handled

## [0.3.6] - 2022-11-24
### Changed
 - Ensure folders are created when skipping a template

## [0.3.5] - 2022-11-24
### Changed
 - Files with wrong encoding skipped when templating a folder

## [0.3.4] - 2022-10-22
### Changed
 - Fix excluded like files being wrongly excluded

## [0.3.3] - 2022-10-20
### Changed
 - Handle python 3.8

## [0.3.2] - 2021-04-25
### Changed
 - Fixed template_file failing when no subfolders

## [0.3.1] - 2021-04-21
### Changed
 - Fixed template_folder failing when no config is found
 - Fixed template folder failing when folder name not templated
 - Fixed template folder not templating when no config found

## [0.3.0] - 2021-04-15
### Added
 - Local higher priority vars file for folder templating

### Changed
 - yaml module renamed jaml (!BREAKING)

## [0.2.1] - 2021-04-15
### Changed
 - Fix python-benedict dependency

## [0.2.0] - 2021-04-15
### Added
 - Add doc to template_string
 - Add template_file to jinja
 - Add template_folder to jinja

### Changed
 - Enhanced dumping
 - Fixed CI not updating packages

## [0.1.9] - 2021-11-30
### Changed
 - fixed dump (again :p)

## [0.1.8] - 2021-11-30
### Changed
 - fixed dump

## [0.1.7] - 2021-11-30
### Added
 - possibility to give a context to the yaml loader

## [0.1.6] - 2021-10-24
### Changed
 - removed print statements

## [0.1.5] - 2021-10-24
### Added
 - Add dumpers for yaml
### Changed
 - fixed readme

## [0.1.4] - 2021-10-24
### Changed
 - fixed readme

## [0.1.3] - 2021-10-24
### Added
- ordered
- yaml.load
- yaml.loads
- constructed
- extractor
