def get_filters(context):
    return {
        'suffix_sub': lambda values: [f'{value}_sub' for value in values],
        'get_val': lambda name: name['val'],
        'merge': lambda l1, l2: l1 + l2
    }