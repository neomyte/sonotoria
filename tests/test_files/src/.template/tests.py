def get_tests(context):
    return {
        'has_underscore': lambda value: '_' in value
    }